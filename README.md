# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Entity Notify is a lightweight tool to send notification e-mails
to admins/moderators when user create/edit/delete selected entities.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/entity_notify>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/entity_notify>


## REQUIREMENTS

Telegram API for Drupal modules <https://www.drupal.org/project/telegram_api>


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configure > System > Advanced Entity Notify
       to configure the module.
    3. Save configuration.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
