<?php

/**
 * @file
 * Install, update and uninstall functions for the entity_notify.
 */

use Drupal\comment\Entity\CommentType;
use Drupal\node\Entity\NodeType;

/**
 * Update node and comment settings.
 */
function entity_notify_update_10001(): void {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('entity_notify.settings');
  $enabled_target_entity_types = $config->get('enabled_target_entity_types');
  $entity_notify_admin = $config->get('entity_notify_admin');
  $entity_notify_roles = $config->get('entity_notify_roles');
  $entity_notify_maillist = $config->get('entity_notify_maillist');
  $entity_notify_telegram = $config->get('entity_notify_telegram');
  $entity_notify_telegram_bottoken = $config->get('entity_notify_telegram_bottoken');
  $entity_notify_telegram_chatids = $config->get('entity_notify_telegram_chatids');
  $entity_notify_telegram_proxy = $config->get('entity_notify_telegram_proxy');
  $entity_notify_telegram_proxy_server = $config->get('entity_notify_telegram_proxy_server');
  $entity_notify_telegram_proxy_login = $config->get('entity_notify_telegram_proxy_login');
  $entity_notify_telegram_proxy_password = $config->get('entity_notify_telegram_proxy_password');

  if (in_array('node', $enabled_target_entity_types, TRUE)) {
    $enabled_target_entity_types = array_diff($enabled_target_entity_types, ['node']);
    $config->set('enabled_target_entity_types', $enabled_target_entity_types);
    $config->save(TRUE);
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('node');
    foreach ($bundles as $bundle => $label) {
      $node_type = NodeType::load($bundle);
      if ($node_type) {
        $node_type->setThirdPartySetting('entity_notify', 'enable', TRUE);
        $node_type->setThirdPartySetting('entity_notify', 'admin', $entity_notify_admin);
        $node_type->setThirdPartySetting('entity_notify', 'roles', $entity_notify_roles);
        $node_type->setThirdPartySetting('entity_notify', 'maillist', $entity_notify_maillist);
        $node_type->setThirdPartySetting('entity_notify', 'telegram', $entity_notify_telegram);
        $node_type->setThirdPartySetting('entity_notify', 'telegram_bottoken', $entity_notify_telegram_bottoken);
        $node_type->setThirdPartySetting('entity_notify', 'telegram_chatids', $entity_notify_telegram_chatids);
        $node_type->setThirdPartySetting('entity_notify', 'telegram_proxy', $entity_notify_telegram_proxy);
        $node_type->setThirdPartySetting('entity_notify', 'telegram_proxy_server', $entity_notify_telegram_proxy_server);
        $node_type->setThirdPartySetting('entity_notify', 'telegram_proxy_login', $entity_notify_telegram_proxy_login);
        $node_type->setThirdPartySetting('entity_notify', 'telegram_proxy_password', $entity_notify_telegram_proxy_password);
        $node_type->save();
      }
    }
  }

  if (in_array('comment', $enabled_target_entity_types, TRUE)) {
    $enabled_target_entity_types = array_diff($enabled_target_entity_types, ['comment']);
    $config->set('enabled_target_entity_types', $enabled_target_entity_types);
    $config->save(TRUE);
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('comment');
    foreach ($bundles as $bundle => $label) {
      $comment_type = CommentType::load($bundle);
      if ($comment_type) {
        $comment_type->setThirdPartySetting('entity_notify', 'enable', TRUE);
        $comment_type->setThirdPartySetting('entity_notify', 'admin', $entity_notify_admin);
        $comment_type->setThirdPartySetting('entity_notify', 'roles', $entity_notify_roles);
        $comment_type->setThirdPartySetting('entity_notify', 'maillist', $entity_notify_maillist);
        $comment_type->setThirdPartySetting('entity_notify', 'telegram', $entity_notify_telegram);
        $comment_type->setThirdPartySetting('entity_notify', 'telegram_bottoken', $entity_notify_telegram_bottoken);
        $comment_type->setThirdPartySetting('entity_notify', 'telegram_chatids', $entity_notify_telegram_chatids);
        $comment_type->setThirdPartySetting('entity_notify', 'telegram_proxy', $entity_notify_telegram_proxy);
        $comment_type->setThirdPartySetting('entity_notify', 'telegram_proxy_server', $entity_notify_telegram_proxy_server);
        $comment_type->setThirdPartySetting('entity_notify', 'telegram_proxy_login', $entity_notify_telegram_proxy_login);
        $comment_type->setThirdPartySetting('entity_notify', 'telegram_proxy_password', $entity_notify_telegram_proxy_password);
        $comment_type->save();
      }
    }
  }
}
