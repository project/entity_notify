<?php

namespace Drupal\entity_notify\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures forms module settings.
 */
class EntityNotifySettingsForm extends ConfigFormBase {

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_notify_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_notify.settings'];
  }

  /**
   * Constructs an EntityNotifySettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_notify.settings');

    $all_entity_types = $this->entityTypeManager->getDefinitions();
    $content_entity_types = [];
    /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_type_options */
    foreach ($all_entity_types as $entity_type) {
      if (($entity_type instanceof ContentEntityTypeInterface)) {
        $content_entity_types[$entity_type->id()] = $entity_type->getLabel();
      }
    }

    unset($content_entity_types['file']);
    unset($content_entity_types['user']);
    unset($content_entity_types['menu_link_content']);
    unset($content_entity_types['path_alias']);
    unset($content_entity_types['shortcut']);
    unset($content_entity_types['node']);
    unset($content_entity_types['comment']);

    $form['enabled_target_entity_types'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Enabled target entity types (node and comment excluded)'),
      '#tree' => TRUE,
      '#description' => $this->t('Node and comment settings are in node type edit or comment type edit settings pages.'),
    ];
    $form['enabled_target_entity_types']['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Target entity types'),
      '#options' => $content_entity_types,
      '#default_value' => $config->get('enabled_target_entity_types') ?: [],
    ];

    _entity_notify_get_email_form($form, FALSE);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('entity_notify.settings')
      ->set('enabled_target_entity_types', array_values(array_filter($form_state->getValue('enabled_target_entity_types')['entity_types'])))
      ->set('entity_notify_admin', $form_state->getValue('entity_notify_admin'))
      ->set('entity_notify_roles', array_values(array_filter($form_state->getValue('entity_notify_roles'))))
      ->set('entity_notify_maillist', $form_state->getValue('entity_notify_maillist'))
      ->set('entity_notify_telegram', $form_state->getValue('entity_notify_telegram'))
      ->set('entity_notify_telegram_bottoken', $form_state->getValue('entity_notify_telegram_bottoken'))
      ->set('entity_notify_telegram_chatids', $form_state->getValue('entity_notify_telegram_chatids'))
      ->set('entity_notify_telegram_proxy', $form_state->getValue('entity_notify_telegram_proxy'))
      ->set('entity_notify_telegram_proxy_server', $form_state->getValue('entity_notify_telegram_proxy_server'))
      ->set('entity_notify_telegram_proxy_login', $form_state->getValue('entity_notify_telegram_proxy_login'))
      ->set('entity_notify_telegram_proxy_password', $form_state->getValue('entity_notify_telegram_proxy_password'))
      ->save();
    $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
  }

}
