<?php

namespace Drupal\Tests\entity_notify\Functional;

use Drupal\comment\Entity\CommentType;
use Drupal\comment\Tests\CommentTestTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

// cspell:ignore llamalovers catcuddlers Cuddlers

/**
 * Tests for the entity_notify module.
 *
 * @group entity_notify
 */
class EntityNotifyCommentSettingsFormTest extends BrowserTestBase {

  use CommentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_notify',
    'node',
    'comment',
  ];

  /**
   * The User used for the test.
   */
  private AccountInterface $adminUser;

  /**
   * The User used for the test.
   */
  private AccountInterface $user;

  /**
   * Settings form url.
   */
  protected Url $settingsRoute;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->user = $this->DrupalCreateUser();

    $this->adminUser = $this->DrupalCreateUser([
      'administer entity_notify configuration',
      'administer content types',
      'administer comment types',
    ]);
    $this->drupalCreateRole([], 'llamalovers', 'Llama Lovers');
    $this->drupalCreateRole([], 'catcuddlers', 'Cat Cuddlers');

    $node_type = 'article';
    $this->createContentType(['type' => $node_type]);
    $this->addDefaultCommentField('node', $node_type);

    $this->settingsRoute = Url::fromRoute('entity.comment_type.edit_form', ['comment_type' => 'comment']);
  }

  /**
   * Tests that the settings page can be reached and saved.
   */
  public function testCommentSettingsPage() {
    $this->drupalLogin($this->user);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $telegram_bot_token = $this->randomMachineName();
    $telegram_chat_id = $this->randomMachineName();
    $edit = [
      'entity_notify_enable' => '1',
      'entity_notify_admin' => '1',
      'entity_notify_roles[llamalovers]' => 'llamalovers',
      'entity_notify_roles[catcuddlers]' => 'catcuddlers',
      'entity_notify_maillist' => 'test@example.com',
      'entity_notify_telegram' => '1',
      'entity_notify_telegram_bottoken' => $telegram_bot_token,
      'entity_notify_telegram_chatids' => $telegram_chat_id,
      'entity_notify_telegram_proxy' => '1',
      'entity_notify_telegram_proxy_server' => '127.0.0.1',
      'entity_notify_telegram_proxy_login' => 'user',
      'entity_notify_telegram_proxy_password' => 'password',
    ];
    $expected_values = [
      'enable' => TRUE,
      'admin' => TRUE,
      'roles' => ['llamalovers', 'catcuddlers'],
      'maillist' => 'test@example.com',
      'telegram' => TRUE,
      'telegram_bottoken' => $telegram_bot_token,
      'telegram_chatids' => $telegram_chat_id,
      'telegram_proxy' => TRUE,
      'telegram_proxy_server' => '127.0.0.1',
      'telegram_proxy_login' => 'user',
      'telegram_proxy_password' => 'password',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('has been updated');

    $comment_type = CommentType::load('comment');
    foreach ($expected_values as $field => $expected_value) {
      $actual_value = $comment_type->getThirdPartySetting('entity_notify', $field);
      $this->assertEquals($expected_value, $actual_value);
    }
  }

}
