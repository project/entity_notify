<?php

namespace Drupal\Tests\entity_notify\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\VocabularyInterface;
use Drupal\Tests\BrowserTestBase;

// cspell:ignore llamalovers catcuddlers Cuddlers

/**
 * Tests for the entity_notify module.
 *
 * @group entity_notify
 */
class EntityNotifyOtherEntitiesNotificationsTest extends BrowserTestBase {

  use AssertMailTrait {
    getMails as drupalGetMails;
  }

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_notify',
    'taxonomy',
  ];

  /**
   * The User used for the test.
   */
  private AccountInterface $adminUser;

  /**
   * The User used for the test.
   */
  private AccountInterface $user;

  /**
   * The User used for the test.
   */
  private AccountInterface $user2;

  /**
   * Settings form url.
   */
  protected Url $settingsRoute;

  /**
   * Taxonomy vocabulary.
   */
  protected VocabularyInterface $vocabulary;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->settingsRoute = Url::fromRoute('entity_notify.settings');

    $this->drupalCreateRole([], 'llamalovers', 'Llama Lovers');
    $this->drupalCreateRole([], 'catcuddlers', 'Cat Cuddlers');

    $this->adminUser = $this->DrupalCreateUser([
      'administer entity_notify configuration',
    ]);

    $this->user = $this->DrupalCreateUser();
    $this->user->addRole('llamalovers');
    $this->user->save();

    $this->user2 = $this->DrupalCreateUser();
    $this->user2->addRole('catcuddlers');
    $this->user2->save();

    $this->vocabulary = Vocabulary::create([
      'name' => 'Camelids',
      'vid' => 'camelids',
    ]);
    $this->vocabulary->save();
  }

  /**
   * Tests notification to admin.
   */
  public function testAdminNotify() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'enabled_target_entity_types[entity_types][taxonomy_term]' => 'taxonomy_term',
      'entity_notify_admin' => '1',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    $captured_emails = $this->drupalGetMails();
    $this->assertCount(0, $captured_emails, 'The captured emails queue is empty.');

    $term = Term::create([
      'name' => 'Llama',
      'vid' => $this->vocabulary->id(),
    ]);
    $term->save();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(1, $captured_emails, 'One email was captured.');

    $term->setName('New name')->save();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(2, $captured_emails, 'Two emails were captured.');

    $term->delete();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(3, $captured_emails, 'Three emails were captured.');
  }

  /**
   * Tests notification to role.
   */
  public function testRoleNotify() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'enabled_target_entity_types[entity_types][taxonomy_term]' => 'taxonomy_term',
      'entity_notify_roles[llamalovers]' => 'llamalovers',
      'entity_notify_roles[catcuddlers]' => 'catcuddlers',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Before we send the email, drupalGetMails should return an empty array.
    $captured_emails = $this->drupalGetMails();
    $this->assertCount(0, $captured_emails, 'The captured emails queue is empty.');

    $term = Term::create([
      'name' => 'Llama',
      'vid' => $this->vocabulary->id(),
    ]);
    $term->save();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(2, $captured_emails, 'One email was captured.');

    $term->setName('New name')->save();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(4, $captured_emails, 'Two emails were captured.');

    $term->delete();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(6, $captured_emails, 'Three emails were captured.');
  }

  /**
   * Tests notification to custom email.
   */
  public function testCustomEmailNotify() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'enabled_target_entity_types[entity_types][taxonomy_term]' => 'taxonomy_term',
      'entity_notify_maillist' => 'test@example.com,test2@example.com',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    $captured_emails = $this->drupalGetMails();
    $this->assertCount(0, $captured_emails, 'The captured emails queue is empty.');

    $term = Term::create([
      'name' => 'Llama',
      'vid' => $this->vocabulary->id(),
    ]);
    $term->save();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(2, $captured_emails, 'One email was captured.');

    $term->setName('New name')->save();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(4, $captured_emails, 'Two emails were captured.');

    $term->delete();
    $captured_emails = $this->drupalGetMails(['key' => 'entity_notify_new_event']);
    $this->assertCount(6, $captured_emails, 'Three emails were captured.');
  }

}
