<?php

namespace Drupal\Tests\entity_notify\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

// cspell:ignore llamalovers catcuddlers Cuddlers

/**
 * Tests for the entity_notify module.
 *
 * @group entity_notify
 */
class EntityNotifySettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_notify',
    'taxonomy',
  ];

  /**
   * The User used for the test.
   */
  private AccountInterface $adminUser;

  /**
   * The User used for the test.
   */
  private AccountInterface $user;

  /**
   * Settings form url.
   */
  protected Url $settingsRoute;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->settingsRoute = Url::fromRoute('entity_notify.settings');
    $this->user = $this->DrupalCreateUser();
    $this->adminUser = $this->DrupalCreateUser([
      'administer entity_notify configuration',
    ]);
    $this->drupalCreateRole([], 'llamalovers', 'Llama Lovers');
    $this->drupalCreateRole([], 'catcuddlers', 'Cat Cuddlers');
  }

  /**
   * Tests that the settings page can be reached and saved.
   */
  public function testSettingsPageExists() {
    $this->drupalLogin($this->user);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $telegram_bot_token = $this->randomMachineName();
    $telegram_chat_id = $this->randomMachineName();
    $edit = [
      'enabled_target_entity_types[entity_types][taxonomy_term]' => 'taxonomy_term',
      'entity_notify_admin' => '1',
      'entity_notify_roles[llamalovers]' => 'llamalovers',
      'entity_notify_roles[catcuddlers]' => 'catcuddlers',
      'entity_notify_maillist' => 'test@example.com',
      'entity_notify_telegram' => '1',
      'entity_notify_telegram_bottoken' => $telegram_bot_token,
      'entity_notify_telegram_chatids' => $telegram_chat_id,
      'entity_notify_telegram_proxy' => '1',
      'entity_notify_telegram_proxy_server' => '127.0.0.1',
      'entity_notify_telegram_proxy_login' => 'user',
      'entity_notify_telegram_proxy_password' => 'password',
    ];
    $expected_values = [
      'enabled_target_entity_types' => ['taxonomy_term'],
      'entity_notify_admin' => TRUE,
      'entity_notify_roles' => ['llamalovers', 'catcuddlers'],
      'entity_notify_maillist' => 'test@example.com',
      'entity_notify_telegram' => TRUE,
      'entity_notify_telegram_bottoken' => $telegram_bot_token,
      'entity_notify_telegram_chatids' => $telegram_chat_id,
      'entity_notify_telegram_proxy' => TRUE,
      'entity_notify_telegram_proxy_server' => '127.0.0.1',
      'entity_notify_telegram_proxy_login' => 'user',
      'entity_notify_telegram_proxy_password' => 'password',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    foreach ($expected_values as $field => $expected_value) {
      $actual_value = $this->config('entity_notify.settings')->get($field);
      $this->assertEquals($expected_value, $actual_value);
    }
  }

}
